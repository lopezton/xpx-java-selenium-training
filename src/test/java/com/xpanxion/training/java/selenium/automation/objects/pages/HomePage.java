package com.xpanxion.training.java.selenium.automation.objects.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.xpanxion.training.java.selenium.automation.Constant;
import com.xpanxion.training.java.selenium.automation.objects.templates.MainPageTemplate;

/**
 * HomePage PageObject Model.
 *
 */
public class HomePage extends MainPageTemplate {

	@FindBy(id = "u3097-6")
	private WebElement taglineText;
	
	/**
	 * Create an instance of the HomePage PageObject using the Selenium WebDriver object. Page
	 * must be loaded, or else WebDriver will fail to retrieve the elements on the page.
	 * 
	 * <p>If the Home Page also needs to be loaded, see <b>HomePage(WebDriver driver, boolean openHomePage)</b>.
	 * 
	 * @see #HomePage(WebDriver, boolean)
	 * @param driver The Selenium WebDriver object to use.
	 */
	public HomePage(WebDriver driver) {
		super(driver);
	}
	
	/**
	 * Create an instance of the HomePage PageObject using the Selenium WebDriver object. Page
	 * will be loaded if <b>openHomePage</b> is <b>true</b>.
	 * 
	 * @param driver The Selenium WebDriver object to use.
	 * @param openHomePage If true, opens http://www.xpanxion.com/. If false, assumes the page
	 * is already open and loaded.
	 */
	public HomePage(WebDriver driver, boolean openHomePage) {
		super(driver, openHomePage ? Constant.INSECURE_APP_URL : null);
	}

	/**
	 * Get the Tagline Text WebElement.
	 * 
	 * @return the Tagline Text WebElement.
	 */
	public WebElement getTaglineText() {
		return taglineText;
	}

	/**
	 * Set the Tagline Text WebElement.
	 * 
	 * @param taglineText the Tagline Text WebElement to set.
	 */
	public void setTaglineText(WebElement taglineText) {
		this.taglineText = taglineText;
	}
	
}

