package com.xpanxion.training.java.selenium.automation.objects.pages;

import org.openqa.selenium.WebDriver;

import com.xpanxion.training.java.selenium.automation.objects.templates.MainPageTemplate;

/**
 * ApproachPage PageObject Model.
 *
 */
public class ApproachPage extends MainPageTemplate {

	/**
	 * Create an instance of the ApproachPage PageObject using the Selenium WebDriver object. Page
	 * must be loaded, or else WebDriver will fail to retrieve the elements on the page.
	 * 
	 * @param driver The Selenium WebDriver object to use.
	 */
	public ApproachPage(WebDriver driver) {
		super(driver);
	}

}
