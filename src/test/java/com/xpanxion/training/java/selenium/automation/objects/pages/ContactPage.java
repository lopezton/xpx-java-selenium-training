package com.xpanxion.training.java.selenium.automation.objects.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.xpanxion.training.java.selenium.automation.beans.ContactDetailBean;
import com.xpanxion.training.java.selenium.automation.objects.templates.MainPageTemplate;

/**
 * ContactPage PageObject Model.
 *
 */
public class ContactPage extends MainPageTemplate {

	@FindBy(id = "widgetu1680_input")
	private WebElement contactName;
	
	@FindBy(id = "widgetu1684_input")
	private WebElement contactEmailAddress;
	
	@FindBy(id = "widgetu1691_input")
	private WebElement contactCellPhoneNumber;
	
	@FindBy(id = "widgetu1674_input")
	private WebElement contactMessage;
	
	/**
	 * Create an instance of the ContactPage PageObject using the Selenium WebDriver object. Page
	 * must be loaded, or else WebDriver will fail to retrieve the elements on the page.
	 * 
	 * @param driver The Selenium WebDriver object to use.
	 */
	public ContactPage(WebDriver driver) {
		super(driver);
	}

	public void clearContactDetails() {
		this.contactName.clear();
		this.contactEmailAddress.clear();
		this.contactCellPhoneNumber.clear();
		this.contactMessage.clear();
	}
	
	public void fillInContactDetails(ContactDetailBean contactDetails) {
		this.contactName.sendKeys(contactDetails.getName());
		this.contactEmailAddress.sendKeys(contactDetails.getEmailAddress());
		this.contactCellPhoneNumber.sendKeys(contactDetails.getCellPhoneNumber());
		this.contactMessage.sendKeys(contactDetails.getMessage());
	}
	
	public WebElement getContactName() {
		return contactName;
	}

	public void setContactName(WebElement contactName) {
		this.contactName = contactName;
	}

	public WebElement getContactEmailAddress() {
		return contactEmailAddress;
	}

	public void setContactEmailAddress(WebElement contactEmailAddress) {
		this.contactEmailAddress = contactEmailAddress;
	}

	public WebElement getContactCellPhoneNumber() {
		return contactCellPhoneNumber;
	}

	public void setContactCellPhoneNumber(WebElement contactCellPhoneNumber) {
		this.contactCellPhoneNumber = contactCellPhoneNumber;
	}

	public WebElement getContactMessage() {
		return contactMessage;
	}

	public void setContactMessage(WebElement contactMessage) {
		this.contactMessage = contactMessage;
	}

}