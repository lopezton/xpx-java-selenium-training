package com.xpanxion.training.java.selenium.automation.tests;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.xpanxion.training.java.selenium.automation.objects.pages.HomePage;
import com.xpanxion.training.java.selenium.core.BaseTest;

/**
 * A simple test class to demonstrate what Selenium is and what it can do. This class
 * extends the BaseTest class, which gives the test access to a Selenium WebDriver object
 * for testing.
 *
 */
public class SimpleHomepageTest extends BaseTest {

	/**
	 * A Test method to test the homepage title validity.
	 */
	@Test
	public void testHomepageTitle() {
		// Visit the homepage
		new HomePage(this.driver, true);

		// Check the title of the page
		final String pageTitle = this.driver.getTitle();
		System.out.println("Page title is: " + pageTitle);

		System.out.println("Checking to see if page title is correct...");
		Assert.assertEquals(pageTitle, "Enterprise Software Consulting Services- Xpanxion");
	}
	
	/**
	 * A Test method to test the tagline text on the homepage.
	 */
	@Test
	public void testHomepageTagline() {
		// Visit the homepage
		final HomePage homePage = new HomePage(this.driver, true);
		
		// Check the text to see if it is the expected text.
		System.out.println("Checking to see if tagline text is correct...");
		Assert.assertEquals(homePage.getTaglineText().getText(), "THINK RURAL.\nLEVERAGE GLOBAL.");
	}
}
