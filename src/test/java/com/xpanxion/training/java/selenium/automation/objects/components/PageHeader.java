package com.xpanxion.training.java.selenium.automation.objects.components;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

import com.xpanxion.training.java.selenium.automation.Constant;
import com.xpanxion.training.java.selenium.core.PageObject;

/**
 * Selenium PageHeader object that models the page header found on http://www.xpanxion.com/
 *
 */
public class PageHeader extends PageObject {

	@FindBy(id = "u11274_img")
	@CacheLookup
	private WebElement headerLogo;
	
	@FindBy(id = "u139-4")
	private WebElement homeLink;
	
	@FindBy(id = "u116-4")
	private WebElement aboutLink;
	
	@FindBy(id = "u129-4")
	private WebElement approachLink;
	
	@FindBy(id = "u123-4")
	private WebElement servicesLink;
	
	@FindBy(id = "u15734-4")
	private WebElement careersLink;
	
	@FindBy(id = "u144-4")
	private WebElement contactLink;
	
	public PageHeader(WebDriver driver) {
		super(driver);
	}

	public WebElement getHeaderLogo() {
		return headerLogo;
	}

	public void setHeaderLogo(WebElement headerLogo) {
		this.headerLogo = headerLogo;
	}

	public WebElement getHomeLink() {
		return homeLink;
	}

	public void setHomeLink(WebElement homeLink) {
		this.homeLink = homeLink;
	}

	public WebElement getAboutLink() {
		return aboutLink;
	}

	public void setAboutLink(WebElement aboutLink) {
		this.aboutLink = aboutLink;
	}

	public WebElement getApproachLink() {
		return approachLink;
	}

	public void setApproachLink(WebElement approachLink) {
		this.approachLink = approachLink;
	}

	public WebElement getServicesLink() {
		return servicesLink;
	}

	public void setServicesLink(WebElement servicesLink) {
		this.servicesLink = servicesLink;
	}

	public WebElement getCareersLink() {
		return careersLink;
	}

	public void setCareersLink(WebElement careersLink) {
		this.careersLink = careersLink;
	}

	public WebElement getContactLink() {
		return contactLink;
	}

	public void setContactLink(WebElement contactLink) {
		this.contactLink = contactLink;
	}
	
	/**
	 * Returns whether or not <b>headerLink</b> is an active header link,
	 * or has an underline style underneath it's text.
	 *  
	 * @param headerLink The header link WebElement to check.
	 * @return <b>true</b> if the underline exists, <b>false</b> otherwise.
	 */
	public static boolean isActiveHeaderLink(WebElement headerLink) {
		final WebElement headerLinkWrapper = headerLink.findElement(By.xpath(".."));
		return headerLinkWrapper.getAttribute(Constant.ATTR_CLASS).contains("MuseMenuActive");
	}
}
