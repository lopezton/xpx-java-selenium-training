package com.xpanxion.training.java.selenium.automation.objects.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.xpanxion.training.java.selenium.automation.objects.templates.MainPageTemplate;

/**
 * AboutPage PageObject Model.
 *
 */
public class AboutPage extends MainPageTemplate {

	@FindBy(id = "u3102-4")
	private WebElement taglineText;
	
	/**
	 * Create an instance of the AboutPage PageObject using the Selenium WebDriver object. Page
	 * must be loaded, or else WebDriver will fail to retrieve the elements on the page.
	 * 
	 * @param driver The Selenium WebDriver object to use.
	 */
	public AboutPage(WebDriver driver) {
		super(driver);
	}

	/**
	 * Get the Tagline Text WebElement.
	 * 
	 * @return the Tagline Text WebElement.
	 */
	public WebElement getTaglineText() {
		return taglineText;
	}

	/**
	 * Set the Tagline Text WebElement.
	 * 
	 * @return the Tagline Text WebElement to set.
	 */
	public void setTaglineText(WebElement taglineText) {
		this.taglineText = taglineText;
	}

}
