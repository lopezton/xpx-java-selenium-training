package com.xpanxion.training.java.selenium.automation;

public class Constant {

	public static final String HTTP_PREFIX = "http://";
	
	public static final String INSECURE_APP_URL = HTTP_PREFIX + "www.xpanxion.com/";
	
	public static final String PAGE_URL_HOME = "index.html";
	
	public static final String PAGE_URL_ABOUT = "about.html";
	
	public static final String PAGE_URL_APPROACH = "approach.html";
	
	public static final String PAGE_URL_SERVICES = "services.html";
	
	public static final String PAGE_URL_CAREERS = "careers.html";
	
	public static final String PAGE_URL_CONTACT = "contact.html";
	
	public static final String ATTR_CLASS = "class";
	
	public static final String ATTR_VALUE = "value";
}
