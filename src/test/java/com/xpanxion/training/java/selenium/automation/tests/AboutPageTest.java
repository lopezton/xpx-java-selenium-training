package com.xpanxion.training.java.selenium.automation.tests;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.xpanxion.training.java.selenium.automation.objects.pages.AboutPage;
import com.xpanxion.training.java.selenium.automation.objects.pages.HomePage;
import com.xpanxion.training.java.selenium.core.BaseTest;

/**
 * A simple test class to demonstrate what Selenium is and what it can do. This class
 * extends the BaseTest class, which gives the test access to a Selenium WebDriver object
 * for testing.
 *
 */
public class AboutPageTest extends BaseTest {

	/**
	 * A Test method to test the tagline text on the About page.
	 */
	@Test
	public void testAboutPageTaglineText() {
		// Visit the about page
		final AboutPage aboutPage = new HomePage(this.driver, true).navToAboutPage();
		
		// Check the text to see if it is the expected text.
		System.out.println("Checking to see if tagline text is correct...");
		Assert.assertEquals(aboutPage.getTaglineText().getText(), "ABOUT US");
	}
}
