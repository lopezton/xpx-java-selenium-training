package com.xpanxion.training.java.selenium.automation.tests.contact;

import org.apache.commons.lang3.StringUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.xpanxion.training.java.selenium.automation.Constant;
import com.xpanxion.training.java.selenium.automation.beans.ContactDetailBean;
import com.xpanxion.training.java.selenium.automation.objects.pages.ContactPage;
import com.xpanxion.training.java.selenium.automation.objects.pages.HomePage;
import com.xpanxion.training.java.selenium.core.BaseTest;

/**
 * Simple Contact Page Test Class
 *
 */
public class ContactUsTest extends BaseTest {
	
	/**
	 * Test to ensure the input fields are present and functional. This is a demonstration test
	 * to instruct about working with user inputs.
	 */
	@Test
	public void testInputFields() {
		
		// Create our contact details bean.
		final ContactDetailBean contactDetails = new ContactDetailBean();
		contactDetails.setName("Daffy Duck");
		contactDetails.setEmailAddress("mightyduck@wb.com");
		contactDetails.setCellPhoneNumber("888-888-8888");
		contactDetails.setMessage("I would like to learn more about your company.");
		
		// Navigate to the contact page.
		final ContactPage contactPage = new HomePage(this.driver, true).navToContactPage();
		
		// Enter the contact details on the page form.
		contactPage.fillInContactDetails(contactDetails);
		
		// Ensure the values were entered into the input fields.
		Assert.assertEquals(contactPage.getContactName().getAttribute(Constant.ATTR_VALUE), contactDetails.getName());
		Assert.assertEquals(contactPage.getContactEmailAddress().getAttribute(Constant.ATTR_VALUE), contactDetails.getEmailAddress());
		Assert.assertEquals(contactPage.getContactCellPhoneNumber().getAttribute(Constant.ATTR_VALUE), contactDetails.getCellPhoneNumber());
		Assert.assertEquals(contactPage.getContactMessage().getAttribute(Constant.ATTR_VALUE), contactDetails.getMessage());
		
		// Clear the contact details on the page form.
		contactPage.clearContactDetails();
		
		// Ensure the values were cleared.
		Assert.assertTrue(StringUtils.isEmpty(contactPage.getContactName().getAttribute(Constant.ATTR_VALUE)));
		Assert.assertTrue(StringUtils.isEmpty(contactPage.getContactEmailAddress().getAttribute(Constant.ATTR_VALUE)));
		Assert.assertTrue(StringUtils.isEmpty(contactPage.getContactCellPhoneNumber().getAttribute(Constant.ATTR_VALUE)));
		Assert.assertTrue(StringUtils.isEmpty(contactPage.getContactMessage().getAttribute(Constant.ATTR_VALUE)));
	}

}
