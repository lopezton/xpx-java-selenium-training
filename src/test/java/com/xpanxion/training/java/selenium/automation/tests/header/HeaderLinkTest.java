package com.xpanxion.training.java.selenium.automation.tests.header;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.xpanxion.training.java.selenium.automation.Constant;
import com.xpanxion.training.java.selenium.automation.objects.components.PageHeader;
import com.xpanxion.training.java.selenium.core.BaseTest;
import com.xpanxion.training.java.selenium.core.utils.WaitUtils;

/**
 * Test Class to test if that the header links are functional.
 *
 */
public class HeaderLinkTest extends BaseTest {

	/**
	 * Test the HOME page link functionality.
	 */
	@Test
	public void testHeaderLinkHome() {
		testHeaderLink("HOME", getPageHeader().getHomeLink(), Constant.INSECURE_APP_URL + Constant.PAGE_URL_HOME);
	}
	
	/**
	 * Test the ABOUT page link functionality.
	 */
	@Test
	public void testHeaderLinkAbout() {
		this.testHeaderLink("ABOUT", getPageHeader().getAboutLink(), Constant.INSECURE_APP_URL + Constant.PAGE_URL_ABOUT);
	}
	
	/**
	 * Test the APPROACH page link functionality.
	 */
	@Test
	public void testHeaderLinkApproach() {
		this.testHeaderLink("APPROACH", getPageHeader().getApproachLink(), Constant.INSECURE_APP_URL + Constant.PAGE_URL_APPROACH);
	}
	
	/**
	 * Test the SERVICES page link functionality.
	 */
	@Test
	public void testHeaderLinkServices() {
		this.testHeaderLink("SERVICES", getPageHeader().getServicesLink(), Constant.INSECURE_APP_URL + Constant.PAGE_URL_SERVICES);
	}
	
	/**
	 * Test the CAREERS page link functionality.
	 */
	@Test
	public void testHeaderLinkCareers() {
		this.testHeaderLink("CAREERS", getPageHeader().getCareersLink(), Constant.INSECURE_APP_URL + Constant.PAGE_URL_CAREERS);
	}
	
	/**
	 * Test the CONTACT page link functionality.
	 */
	@Test
	public void testHeaderLinkContact() {
		this.testHeaderLink("CONTACT", getPageHeader().getContactLink(), Constant.INSECURE_APP_URL + Constant.PAGE_URL_CONTACT);
	}
	
	/**
	 * Load the page and prepare the PageHeader object for the test.
	 * 
	 * @return The PageHeader object to test.
	 */
	private PageHeader getPageHeader() {
		this.driver.get(Constant.INSECURE_APP_URL);
		return new PageHeader(this.driver);
	}
	
	/**
	 * Tests to make sure the header link identified by <b>elementId</b> is functional. Then checks
	 * that the selected link is underlined.
	 *  
	 * @param pageName The name of the page. (Used for logging)
	 * @param elementId The HTML id of the Header Link element
	 * @param expectedUrl The expected URL to navigate to, after clicking the Header Link.
	 */
	private void testHeaderLink(String pageName, WebElement element, String expectedUrl) {
		// Get the HOME link element and click it.
		element.click();

		// Perform a wait until the link element is visible, with a one-second delay to
		// allow the page to navigate away and refresh the PageOhject elements.
		WaitUtils.waitUntilElementIsVisible(this.driver, element);
		
		// Ensure we have navigated to the new and correct page.
		Assert.assertEquals(this.driver.getCurrentUrl(), expectedUrl);
		
		// Ensure that the link contains the highlighting css class.
		Assert.assertTrue(PageHeader.isActiveHeaderLink(element), 
				"The " + pageName + " Link was clicked, but on page load link was not marked as active");
	}
	
}
