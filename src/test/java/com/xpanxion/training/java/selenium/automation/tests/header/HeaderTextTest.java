package com.xpanxion.training.java.selenium.automation.tests.header;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.xpanxion.training.java.selenium.automation.objects.components.PageHeader;
import com.xpanxion.training.java.selenium.core.BaseTest;

/**
 * Test Class to test if the text displayed in the header is correct.
 *
 */
public class HeaderTextTest extends BaseTest{

	/**
	 * Test method navigates to the homepage and checks to ensure that all links in the 
	 * header are correct with respect to the Header requirements.
	 */
	@Test
	public void testCorrectHeaderText() {
		// Navigate to the home page
		this.driver.get("http://www.xpanxion.com/");
		
		final PageHeader pageHeader = new PageHeader(this.driver);
		
		// Check the Home Text
		Assert.assertEquals(pageHeader.getHomeLink().getText(), "HOME");

		// Check the About Text
		Assert.assertEquals(pageHeader.getAboutLink().getText(), "ABOUT");
		
		// Check the Approach Text
		Assert.assertEquals(pageHeader.getApproachLink().getText(), "APPROACH");
		
		// Check the Services Text
		Assert.assertEquals(pageHeader.getServicesLink().getText(), "SERVICES");
		
		// Check the Careers Text
		Assert.assertEquals(pageHeader.getCareersLink().getText(), "CAREERS");
		
		// Check the Contact Text
		Assert.assertEquals(pageHeader.getContactLink().getText(), "CONTACT");
	}
}
