package com.xpanxion.training.java.selenium.core;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

/**
 * This class is the main testing class. Extend this class from a test class to
 * create a WebDriver object and gain access to it to use for testing.
 */
public class BaseTest {

	/**
	 * The WebDriver object for use in testing. Since the scope is protected,
	 * the object will be available to all sub-classes.
	 */
	protected WebDriver driver;

	/**
	 * This method is executed before a test method begins, using TestNG's @BeforeMethod
	 * annotation. This method is primarily responsible for obtaining a unique
	 * WebDriver object for the test to use.
	 */
	@BeforeMethod
	public void setup() {
		this.driver = new FirefoxDriver();
	}

	/**
	 * This method is executed after a test method has completed, using TestNG's @AfterMethod
	 * annotation. This method is primarily responsible for taking care of all
	 * clean up tasks, so that more tests may be run.
	 */
	@AfterMethod
	public void teardown() {
		this.driver.quit();
	}
}
