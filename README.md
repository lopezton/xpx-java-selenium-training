# Xpanxion Java Selenium Training

## About this Tutorial
This tutorial is intended to introduce a novice Java automation developer to the Selenium library, as well as several industry-standard tools used in automation projects.  The tutorial assumes a basic knowledge of Java syntax and will attempt to steer away from advanced Java concepts early on and gradually introduce more to the student, later on.  

In addition to being able to follow through this tutorial via the Wiki content (See link below), the intent is to present this material in a structured series of courses available to students in a classroom setting.  We want to ensure that the material and concepts provided by this course are well understood for a wide variety of students seeking to learn more about automation in Java.  

In the beginning, the Wiki tutorial takes a very simple approach and attempts to put the student in the driver seat, walking him or her step-by-step through each line of code. By doing this, we will be able to illustrate the purpose behind every piece of logic, making the student understand as if he or she were writing the code.  At the end of each lesson will be an optional (but highly encouraged) try-it-yourself section to further aid in the educational experience.  

As we progress, we will get away from the simple approach and allow the students to branch off and go in their own direction to develop their own unique Automated frameworks. Students will be able to introduce new libraries, or shift their framework more towards a data-driven, feature-driven, or any-other-kind-of-driven framework they would like.  

## Let's Get Started!
To get started: Check out the project WIKI, here: https://bitbucket.org/lopezton/xpx-java-selenium-training/wiki/Home